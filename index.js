/*
This Telegram bot helps you find fake news regarding Corona. For more infos, take a look at the README.md.
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
PREP
*/

// Quickfix for known bug in Telegram communication
process.env["NTBA_FIX_319"] = 1

// imports
const fs = require('fs')
const https = require('https')
const TelegramBot = require('node-telegram-bot-api')
const parseFakeNews = require("./src/parser")

// Load API keys
var keys = require('./data/keys.json')

// Load static message strings
var messages = require('./data/messages.json')

// Create Telegram bot
const bot = new TelegramBot(keys.telegram, {polling: true})

// start notification
console.log("Starting Fakti-Bot")


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Send hilfe / start message
bot.onText(/\/(hilfe|help|Hilfe|Help|start|Start)/, (msg, match) => {
  var chatId = msg.chat.id
  bot.sendMessage(chatId, messages.help[0], { parse_mode: "HTML" }).then(() => {
      bot.sendMessage(chatId, messages.help[1], { parse_mode: "HTML" })
  }).then(() => {
      bot.sendMessage(chatId, messages.help[2], { parse_mode: "HTML" })
  })
})

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Send report infos
bot.onText(/\/(report|Report|melden|Melden)/, (msg, match) => {
  var chatId = msg.chat.id
  let response = messages.report
  bot.sendMessage(chatId, response, { parse_mode: "HTML", disable_web_page_preview: true})
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Send info about fake news
bot.onText(/\/(play|Play|spielen|Spielen)/, (msg, match) => {
  var chatId = msg.chat.id
  let response = messages.play
  bot.sendMessage(chatId, response, { parse_mode: "HTML" })
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Send info about fake news
bot.onText(/\/(info|Info)/, (msg, match) => {
  var chatId = msg.chat.id
  bot.sendMessage(chatId, messages.info[0], { parse_mode: "HTML", disable_web_page_preview: true}).then(() => {
      bot.sendMessage(chatId, messages.info[1], { parse_mode: "HTML", disable_web_page_preview: true})
  })
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Subscribe
bot.onText(/\/(subscribe|Subscribe|abonnieren|Abonnieren)/, (msg, match) => {
  var chatId = msg.chat.id
  let response = messages.subscribe
  bot.sendMessage(chatId, response, { parse_mode: "HTML", disable_web_page_preview: true})
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// About
bot.onText(/\/(about|About|über|Über|Impressum|impressum)/, (msg, match) => {
  var chatId = msg.chat.id
  let response = messages.about
  bot.sendMessage(chatId, response, { parse_mode: "HTML", disable_web_page_preview: true})
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Checks without keywords
bot.onText(/\/check/, (msg, match) => {
    if (msg.text == "/check") {
        var chatId = msg.chat.id
        let response = messages.check
         bot.sendMessage(chatId, response, { parse_mode: "HTML", disable_web_page_preview: true})
    }
})

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Checks without keywords
// bot.onText(/(.+)/, (msg, match) => {

//     var chatId = msg.chat.id

//     let keywords = ["/hilfe", "/help", "/Hilfe", "/Help", "/start", "/Start", "/report", "/Report", "/melden", "/Melden", "/play", "/Play", "/spielen", "/Spielen", "/info", "/Info", "/subscribe", "/Subscribe", "/abonnieren", "/Abonnieren", "/about", "/About", "/über", "/Über", "/Impressum", "/impressum", "/check"]
//     let thanks = ["danke", "vielen dank"]
//     let thanksAnswer = ["Immer wieder gerne!", "Klar doch!", "Bitte sehr! 😊", "Nicht dafür! 😊", "Bitte!", "Bit­te­schön!"]
//     let hello = ["hallo", "guten tag", "hey", "hi"]
//     let helloAnswer = ["Hallo 👋", "Hallo, schön von dir zu hören!", "Guten Tag 😊"]
//     let errorAnswer = ["Bip, Bib, Bop. Ich bin ein Bot. 🤖\nLeider kann ich deine Anfrage nicht verstehen.", "Ähm... 🤨\nEs scheint so, als würde ich nicht genug 'Mensch' sprechen, um dich zu verstehen.", "Oh. Leider scheinen bei mir Kabel verknotet zu sein. Ich kann dich nicht verstehen. 🤔", "Interner Botalarm 🚨 - Unbekannte Anfrage eingegangen!\nLeider verstehe ich dich nicht.  🥺"]

//     if (!keywords.includes(msg.text)) {
//         let msgLower = msg.text.toLowerCase()

//         // Thanks
//         if (thanks.includes(msgLower)) {
//             let response = thanksAnswer[Math.floor(Math.random() * thanksAnswer.length)]
//             bot.sendMessage(chatId, response, {parse_mode: "HTML"})
//         }

//         // Hello
//         else if (hello.includes(msgLower)) {
//             let response = helloAnswer[Math.floor(Math.random() * helloAnswer.length)]
//             bot.sendMessage(chatId, response, {parse_mode: "HTML"})
//         }

//         else {
//             let response = errorAnswer[Math.floor(Math.random() * errorAnswer.length)] + "\n\n" + messages.error
//             bot.sendMessage(chatId, response, {parse_mode: "HTML"})
//         }
//     }
// })

// bot.onText(/(#WirvsVirus|#wirvsvirus|#WirVsVirus|#wirvsvirushack|#WirVsVirusHack)/, (msg) => {
//     var chatId = msg.chat.id
//     bot.sendPhoto(chatId, "img/wirvsvirus.jpg").then(() => {
//         let response = messages.wirvsvirus
//         bot.sendMessage(chatId, response, {parse_mode: "HTML", disable_web_page_preview: true})
//     })
// })

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Transforms user request for fake news into request string for Mimikama
function createRequestURL(match, site) {
  match = match.replace(/[^0-9a-z\s-äöüß]/gi, '')
  match = match.replace(/\s/g, "+")

  let requestURL = site + match
  return(requestURL)
}

// send response list of found fake news
function sendTelegramFakeNewsList(fakenews, match, requestURL, chatId) {
    let response =  ""

    // Articles found
    if (fakenews.articles.length > 0) {
        response = "🔎 Zu deiner Suchanfrage '" + match + "' habe ich die folgenden Beiträge gefunden. Weitere Ergebnisse findest du z.B. beim " + "<a href='" + requestURL + "'>Correctiv</a>."
        bot.sendMessage(chatId, response, {parse_mode: "HTML", disable_web_page_preview: true}).then(() => {
            let maxArticles = 3
            fakenews.articles.forEach((article, i) => {
                if (i < maxArticles) {
                    response = `<b><a href="${article.href}">${article.title}</a></b>\n`
                    response += `<i>${article.dateString}</i>\n\n`
                    response += article.description + "\n"
                    response += article.ratingDescription + "\n\n"
                    response += "👉 Fazit: Solche Nachrichten sind <b>" + article.rating.replace(/^\w/, c => c.toLowerCase()) + "</b>.\n\n"
                    bot.sendMessage(chatId, response, {parse_mode: "HTML"});

                }
            })
        })
    }

    // No articles found
    else {
        response =  "🧐"
        bot.sendMessage(chatId, response, {parse_mode: "HTML"}).then(() => {
            response =  messages.noArticles[0] + match + messages.noArticles[1]
            bot.sendMessage(chatId, response, {parse_mode: "HTML"})
        })
    }

    return

}


// Find fake news on correctiv.org
bot.onText(/\/check (.+)/, (msg, match) => {

  const chatId = msg.chat.id;
  match = match[1];

  bot.sendMessage(chatId, "⏳", {parse_mode: "HTML"})


  let requestURL = createRequestURL(match, "https://correctiv.org/?s=")

  https.get(requestURL, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
          data += chunk
        });
        resp.on('end', () => {
            let fakenews = parseFakeNews(data)
            sendTelegramFakeNewsList(fakenews, match, requestURL, chatId)
        })
    })

});


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// logging
var requestCounter = 0 // counts the total number of requests


bot.on('message', message => {
    requestCounter++
    console.log(`Request Nr. ${requestCounter}: ${Date()} --- ${message.text}`)
})