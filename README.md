# Fakti - Der Telegram-Bot im Kampf gegen Fake News
Fakti entstand im Rahmen des [Hackathons der Bundesregierung #wirvsvirus](https://www.bundesregierung.de/breg-de/themen/coronavirus/wir-vs-virus-1731968) (Herausforderung #1_021_krisenkommunikation).

## Über
Mit Fakti kannst du Neuigkeiten direkt im Messenger auf ihre Glaubwürdigkeit prüfen. Lass Fakti einfach über ein Schlagwort nach einem passenden Faktencheck suchen.
Du kannst Fakti sogar einem Gruppenchat hinzufügen und ihn dort direkt Fake News enttarnen lassen. Fakti bietet Dir eine Übersicht der Faktenchecks von [correctiv.org](correctiv.org). Außerdem bietet dir Fakti im Chat noch viele weitere Möglichkeiten dich zu informieren und deine Skills im Umgang mit Fake News zu verbessern.
Selbstverständlich kannst du auch mögliche Falschmeldungen zur Überprüfung melden!

## Befehle für den Bot
* Prüfen, ob es schon eine verifizierte. Einschätzung für eine Meldung gibt

    ```
    /check <Suchbegriff_1, Suchbegriff_2, ...>
    ```

* Mehr über Fake News und den Umgang mit ihnen erfahren

    ```
    /info
    ```

* Über aktuell kursierende Falschmeldungen informiert werden

    ```
    /subscribe
    ```

* Spielerisch deine Skills im Umgang mit Fake News testen

    ```
    /play
    ```

* Eine Nachricht zur Überprüfung melden

    ```
    /report
    ```

* Mehr zu Fakti erfahren

    ```
    /about
    ```


## Start
Der Bot ist eine NodeJS-Applikation, die sich mit diesen Schritten starten lässt:

1. Stelle sicher, dass [NodeJS](https://nodejs.org/) und [npm](https://www.npmjs.com/) installiert sind.
2. Installiere alle Dependencies (z.B. die Node-Telegram-Bot-Api).

    ```
    npm install
    ```

3. Registriere beim Telegram-Botfather einen Bot und speichere den Token Deines Bots
im folgenden Format in eine json-Datei unter  ```data/keys.json```
    
    ```
    {"telegram": "Your key from Botfather"}
    ```

4. Bot starten

    ```
    npm start
    ```

## Docker
Der Bot lässt sich auch in einem Docker-Container starten:

    docker build -t <your username>/fakti-bot .
    docker run -p 49160:8080 -d <your username>/fakti-bot

## Links

* Weitere Informationen zur Funktionsweise des Bots findest Du auf unserer [Website](https://faktibot.de).
* Hier ist unsere Projektseite bei [Devpost](https://devpost.com/software/telegram-fake-news-bot).
* Eine Projektvorstellung findest du auf [Youtube](https://www.youtube.com/watch?v=3yxBGQPodno).

Dieses Projekt entstand mit freundlicher Unterstützung von [correctiv.org](correctiv.org).

## Lizenz

Copyright (c) 2020

Licensed under the [MIT license](LICENSE).
