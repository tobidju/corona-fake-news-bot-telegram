/*
This is a very simple hmtl-parser to parse the entries of "https://correctiv.org/?s[...]"
This code should be modified if the structure of the webpage changes. (Unfortunately,
there is not API for corrective-org yet).

The function returns an object like
{
    articles: [
        {
            title: "Nein, die WHO hat keine Hinweise, dass das Coronavirus über Lebensmittel übertragen wird",
            description: "Gibt es neue Erkenntnisse zur Übertragung des Coronavirus Covid-19 [...]",
            img: "href eines Bildes",
            href: "link zum artikel",
            rating: "teilweise falsch",
            ratingDescription: "Teilweise falsch. Es gibt bisher keine Hinweise, dass [...]",
            date: "27. Februar 2020"
        },
        {
            // more articles with the same format
        }
    ]
}
*/

// parser configuration
var parserConfig = {
    resultListSelector: ".list--search",
    newsContainerType: "ARTICLE", // children of seachlistClassSelector which hold the news are of this type
    mustHaveSelector: ".topline > span", // found articles are processed if we could select a element with this selector inside only
    removeSelector: ".list--related", // this element will be removed
    titleSelector: ".tertiary-title",
    linkSelector: ".js-list-title",
    descriptionSelector: ".detail__content > p:first-child > strong",
    ratingSelector: ".topline span",
    ratingDescriptionSelector: ".detail__rating-text",
    imgSelector: ".picture__image ",
    dateSelector: "time.detail__date"
}

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

module.exports = function parse(data) {
    // defaults for parsed data from webpage
    let fakenews = {
      articles: []
    }
  
    // extract container with all search results
    const dom = new JSDOM(data)
    fakenewsContainer = dom.window.document.querySelector(parserConfig.resultListSelector)
    if(fakenewsContainer) // if fake news found
      fakenewsContainer.childNodes.forEach(news => {
        if(news && news.nodeName == parserConfig.newsContainerType) {// if <article> element
          // remove related content if necessary
          let related = news.querySelector(parserConfig.removeSelector)
          if (related)
            related.remove()
  
          if(news.querySelector(parserConfig.mustHaveSelector)) { // if is mustHaveSelector found
    
            // ==== extraction and process data from html
            // title
            let title = news.querySelector(parserConfig.titleSelector)
            // rating
            let rating = news.querySelector(parserConfig.ratingSelector)
            // ratingDescription
            let ratingDescription = news.querySelector(parserConfig.ratingDescriptionSelector)
            // link to full article
            let href = news.querySelector(parserConfig.linkSelector)
            // short description
            let description = news.querySelector(parserConfig.descriptionSelector)
            // url to image
            let img = news.querySelector(parserConfig.imgSelector)
            // date string
            let dateString = news.querySelector(parserConfig.dateSelector)
  
            // === preprocess
            if(title && rating && ratingDescription && href && description && img && dateString){
              // title
              title = title.textContent
              // rating
              rating = rating.textContent
              // ratingDescription
              ratingDescription = ratingDescription.textContent
              ratingDescription = ratingDescription.trim().slice(18).trim()
              // href
              href = href.href
              // description
              description = description.textContent.trim()
              // img
              img = img.outerHTML
              img = img.match(/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/g)[0]
              // dateString
              dateString = dateString.textContent
            }
    
    
            // === add acticle data to result
            fakenews.articles.push({
              title,
              href,
              description,
              rating,
              ratingDescription,
              img,
              dateString
            })
          }
        }
  
      })
  
    return(fakenews)
  }