FROM node:10

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# If we are building our code for development
#RUN npm install
# If we are building our code for production
RUN npm ci --only=production

# Bundle app source
COPY . .

# Expose node port
EXPOSE 8080
CMD [ "node", "index.js" ]